use leptos::*;

#[component]
// fn ProgressBar(
    cx: Scope,
    progress: ReadSignal<i32>
) -> impl IntoView {
    view! { cx,
        <progress
            max="50"
            // not working in 2.5
            // value=progress
            value=progress.get()
        />
    }
}

#[component]
fn App(cx: Scope) -> impl IntoView {
    let (count, set_count) = create_signal(cx, 0);
    let double_count = move || count.get() * 2;
    view! { cx, 
        <button
            class=("red", move || count.get() % 2 == 1)
            class=("blue", move || count.get() % 2 == 0)
            on:click=move |_|  {
                set_count.update(|n| *n += 1);
            }
        >
            "Click me: "
            {move || count.get()}
        </button>
        <progress
            max="50"
            // signals are functions, so this <=> `move || count.get()`
            value=double_count
        />
        <p>
            "Double Count: "
            // and again here
            {double_count}
        </p>
        // <ProgressBar/>
        <ProgressBar progress=count/>
        // <ProgressBar progress=double_count/>
    }
}

fn main() {
    mount_to_body(|cx| view! { cx,  <p>"Hello, world!"</p> });
    mount_to_body(|cx| view! { cx,  <p>"AAA"</p> });
    mount_to_body(|cx| view! { cx,  <p>"BBB"</p> });
    mount_to_body(|cx| view! { cx,  <App/> });
}
