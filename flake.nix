{
  description = "test";
  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    rust-overlay = { url = "github:oxalica/rust-overlay"; inputs.nixpkgs.follows = "nixpkgs"; };
    # rust-overlay = { url = "github:oxalica/rust-overlay"; };
  };
  outputs = {
    self,
    nixpkgs,
    flake-utils,
    rust-overlay,
  }:
  (flake-utils.lib.eachDefaultSystem (system: let
    # overlays = [rust-overlay.overlays.default];
    overlays = [(import rust-overlay)];
    pkgs = import nixpkgs {inherit system overlays; };
    # pkgs = import nixpkgs {inherit system; };
    # rust = pkgs.rust-bin.fromRustupToolchainFile ./rust-toolchain.toml;
    # rust = pkgs.rust-bin.beta.latest.default;
    # rust = pkgs.rust-bin.stable.latest.default;
    # rust = pkgs.rust-bin.stable."1.69.0".default;
    rust = pkgs.rust-bin.stable."1.68.0".default.override {
      # extensions = [ "rust-src" ];
      targets = [ "wasm32-unknown-unknown" ];
    };
  in {
    formatter = pkgs.alejandra;
    apps = rec {
      cargo = {
        type = "app";
        program = "${pkgs.cargo}/bin/cargo";
      };
      default = cargo;
    };
    devShells.default = pkgs.mkShell {
      buildInputs = with pkgs; [
        # cargo
        cargo-outdated
        openssl
        pkg-config
        rust
        # rustc
        # rustfmt
        # (rust-bin.stable.latest.default.override {
        #       extensions = [ "rust-src" ];
        #       targets = [ "wasm32-unknown-unknown" ];
        # })
        trunk
        # wasm-bindgen-cli
      ];
      RUST_BACKTRACE = 1;
    };
  }));
}
